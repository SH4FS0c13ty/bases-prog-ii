# -*- coding: utf-8 -*-

def saisie_intervalle(mini, maxi):    
    n=-1
    while n < mini or n > maxi:
        n=int(input("Saisissez un nb entre " + str(mini) + " et " + str(maxi) + ": "))
    return n

val=saisie_intervalle(0, 10)
print(val)