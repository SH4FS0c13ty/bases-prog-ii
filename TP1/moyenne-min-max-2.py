# -*- coding: utf-8 -*-

def minmax(note):
    global nmin, nmax
    if nmin == -1 or nmax == -1:
        nmin=note
        nmax=note
    elif note<nmin:
        nmin=note
    elif note>nmax:
        nmax=note

def saisie_intervalle(mini, maxi):    
    n=-1
    while n < mini or n > maxi:
        n=int(input("Saisissez un nb entre " + str(mini) + " et " + str(maxi) + ": "))
    return n


def main():
    global nmin, nmax
    nmin,nmax=-1,-1
    moy,i=0,0
    mini, maxi=0,20
    print("Enter any non-number character to exit.")
    try:
        while True:
            v=saisie_intervalle(mini,maxi)
            if v >= mini and v <= maxi:
                moy+=v
                minmax(v)
                i+=1
    except ValueError:
        print("Exiting...")
        
    moy=moy/i
    
    
    print("Moyenne:", str(moy))
    print("Min:", str(nmin))
    print("Max:", str(nmax))

if __name__ == '__main__':
    main()