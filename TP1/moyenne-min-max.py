# -*- coding: utf-8 -*-

n,moy,i=0,0,0
global nmin, nmax
nmin,nmax=-1,-1

def minmax(note):
    global nmin, nmax
    if nmin == -1 or nmax == -1:
        nmin=note
        nmax=note
    elif note<nmin:
        nmin=note
    elif note>nmax:
        nmax=note

while n>=0 and n<=20:
    n=int(input("Entrez la note: "))
    if n<0 or n>20:
        break
    moy+=n
    minmax(n)
    i+=1
    
moy=moy/i
print("Moyenne:", str(moy))
print("Min:", str(nmin))
print("Max:", str(nmax))