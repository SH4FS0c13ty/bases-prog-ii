# -*- coding: utf-8 -*-
import ipv4_str

def cherche_domaine(dom, lst):
    """ Find IPv4 address corresponding to submitted URI in the submitted list """
    res=None
    if type(lst) != list or len(lst[0]) != len(lst[1]) or type(dom) != str:
        return None
    for i in range(0,1):
        for j in range(0,len(lst[i])):
            if lst[0][j] == dom:
                res=lst[1][j]
    if res is None:
        return None
    ip=ipv4_str.ipv4_vers_chaine(res)
    return ip
    

def main():
    list_test=[["univ-amu.fr", "enseignementsup-recherche.gouv.fr", "seenthis.net"],
               [[139,124,244,38], [185,75,143,29], [217,182,178,243]]]
    dom="univ-amu.fr"
    result=cherche_domaine(dom, list_test)
    #if result is None:
    #    result="Domain not found"
    #print(dom, "=>", result)
    assert result == "139.124.244.38"
    print("Valid domain search [OK]")
    
    list_test=[["univ-amu.fr", "enseignementsup-recherche.gouv.fr", "seenthis.net"],
               [[139,124,244,38], [185,75,143,29], [217,182,178,243]]]
    dom="seenthis.net"
    result=cherche_domaine(dom, list_test)
    assert result == "217.182.178.243"
    print("Valid domain search [OK]")
    
    list_test=[["univ-amu.fr", "enseignementsup-recherche.gouv.fr", "seenthis.net"],
               [[139,124,244,38], [185,75,143,29], [217,182,178,243]]]
    dom="univ-amu.f"
    result=cherche_domaine(dom, list_test)
    assert result == None
    print("Invalid domain search [OK]")
    
    list_test=[["enseignementsup-recherche.gouv.fr", "seenthis.net"],
               [[139,124,244,38], [185,75,143,29], [217,182,178,243]]]
    dom="enseignementsup-recherche.gouv.fr"
    result=cherche_domaine(dom, list_test)
    assert result == None
    print("Invalid list length [OK]")
    
    list_test=[["univ-amu.fr", "enseignementsup-recherche.gouv.fr", "seenthis.net"],
               [[139,124,244,"bruh"], [185,75,143,29], [217,182,178,243]]]
    dom="univ-amu.fr"
    result=cherche_domaine(dom, list_test)
    assert result == None
    print("Invalid IP in list [OK]")
    
    list_test=[["univ-amu.fr", "enseignementsup-recherche.gouv.fr", "seenthis.net"],
               ["univ-amu.fr", [185,75,143,29], [217,182,178,243]]]
    dom="univ-amu.fr"
    result=cherche_domaine(dom, list_test)
    assert result == None
    print("Invalid IP in list (type str) [OK]")
    
    list_test=[["univ-amu.fr", "enseignementsup-recherche.gouv.fr", "seenthis.net"],
               [[139,124,244,38.0], [185,75,143,29], [217,182,178,243]]]
    dom="univ-amu.fr"
    result=cherche_domaine(dom, list_test)
    assert result == None
    print("Invalid IP in list (type float) [OK]")
    
    list_test=[[[139,124,244,38], "enseignementsup-recherche.gouv.fr", "seenthis.net"],
               [[139,124,244,38], [185,75,143,29], [217,182,178,243]]]
    dom=[139,124,244,38]
    result=cherche_domaine(dom, list_test)
    assert result == None
    print("Invalid URI in list (type list) [OK]")
    
    list_test=65
    dom="univ-amu.fr"
    result=cherche_domaine(dom, list_test)
    assert result == None
    print("Invalid submitted arguments [OK]")
    
    

if __name__ == '__main__':
    main()