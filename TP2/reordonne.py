# -*- coding: utf-8 -*-
def reordonne(l):
    """ Reorder number using their position mentionned in the n+1 case of the list """
    if type(l) != list or len(l) % 2 != 0:
        return None
    lordonnee=[None]*int(len(l)//2)
    for i in range(0,len(l),2):
        if (l[i+1]) > len(l)//2 or type(l[i+1]) != int:
            return None
        lordonnee[(l[i+1]-1)]=l[i]
        
    return lordonnee
    

def main():
    list_test=[10,3,12,4,36,1,44,2]
    lordonnee=reordonne(list_test)
    assert lordonnee == [36, 44, 10, 12]
    print("Reorder with valid numbers and positions [OK]")
    
    list_test=["bruh",3,12,4,36,1,44,2]
    lordonnee=reordonne(list_test)
    assert lordonnee == [36, 44, "bruh", 12]
    print("Reorder with invalid numbers and positions [OK]")
    
    list_test=[10,3,12,4,36,1,44,5]
    lordonnee=reordonne(list_test)
    assert lordonnee == None
    print("Reorder with valid numbers and invalid positions [OK]")
    
    list_test=12
    lordonnee=reordonne(list_test)
    assert lordonnee == None
    print("Reorder with invalid submitted type [OK]")
    
    list_test=[10,3,12,4,36,1,5]
    lordonnee=reordonne(list_test)
    assert lordonnee == None
    print("Reorder with invalid length [OK]")
    
    list_test=[10,3,12,4,36,1,2.5]
    lordonnee=reordonne(list_test)
    assert lordonnee == None
    print("Reorder with invalid position [OK]")

if __name__ == '__main__':
    main()