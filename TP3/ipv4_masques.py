# -*- coding: utf-8 -*-
def genere_masque(cidr):
    """ Generate an IPv4 mask based on submitted CIDR """
    mask=""
    masktab=[None, None, None, None]
    if type(cidr) is not int or cidr <= 0 or cidr >= 32:
        return None
    for i in range(0,cidr):
        mask+="1"
    for i in range(0,32-len(mask)):
        mask+="0"
    for n in range(0,4):
        pos=8
        masktab[n]=mask[:pos]
        mask=mask[pos:]
    for i in range(0,len(masktab)):
        masktab[i]=int(str(masktab[i]), 2)
        
    return masktab
    

def main():
    cidr_test=19
    mask=genere_masque(cidr_test)
    assert mask == [255, 255, 224, 0]
    print("Test with valid number [OK]")
    
    cidr_test=32
    mask=genere_masque(cidr_test)
    assert mask == None
    print("Test with invalid number #1 [OK]")
    
    cidr_test=-5
    mask=genere_masque(cidr_test)
    assert mask == None
    print("Test with invalid number #2 [OK]")
          
    cidr_test=0
    mask=genere_masque(cidr_test)
    assert mask == None
    print("Test with invalid number #3 [OK]")
          
    cidr_test="hello"
    mask=genere_masque(cidr_test)
    assert mask == None
    print("Test with invalid type (str) #1 [OK]")
    
    cidr_test=[21,25]
    mask=genere_masque(cidr_test)
    assert mask == None
    print("Test with invalid type (lst) #2 [OK]")
    
    cidr_test=19.0
    mask=genere_masque(cidr_test)
    assert mask == None
    print("Test with invalid type (float) #3 [OK]")
    

if __name__ == '__main__':
    main()