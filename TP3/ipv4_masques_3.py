# -*- coding: utf-8 -*-
import check_ipv4

def genere_masque(cidr):
    """ Generate an IPv4 mask based on submitted CIDR """
    mask=""
    masktab=[None, None, None, None]
    if type(cidr) is not int or cidr < 0 or cidr > 32:
        return None
    for i in range(0,cidr):
        mask+="1"
    for i in range(0,32-len(mask)):
        mask+="0"
    for n in range(0,4):
        pos=8
        masktab[n]=mask[:pos]
        mask=mask[pos:]
    for i in range(0,len(masktab)):
        masktab[i]=int(str(masktab[i]), 2)
        
    return masktab
    
def hote(adr, cidr):
    """ Get the host part of an IPv4 address using submitted mask """
    if type(adr) != list or type(cidr) != list or len(adr) != 4 or len(cidr) != 4 or adr[0] == 255:
        return None
    check=check_ipv4.check_ipv4(adr)
    if check == False:
        return None
    for i in range(0,len(adr)):
        adr[i]=format(adr[i], '08b')
        cidr[i]=format(cidr[i], '08b')
        for j in range(0,len(adr[i])):
            if cidr[i][j] == "1":
                adr[i] = adr[i][j:]
        for j in range(0,len(adr)):
            if len(adr[i]) < 8:
                adr[i] = "0" * (8-len(adr[i])) + adr[i]
        # Next loop is to convert bin to dec in list
        for j in range(0,8-len(adr[i])):
            adr[i] += "0"
        adr[i]=int(str(adr[i]), 2)
    
    return adr

def hote_bit_a_bit(adr, cidr):
    """ Get the host part of an IPv4 address using submitted mask """
    if type(adr) != list or type(cidr) != list or len(adr) != 4 or len(cidr) != 4 or adr[0] == 255:
        return None
    check=check_ipv4.check_ipv4(adr)
    if check == False:
        return None
    for i in range(0,len(adr)):
        adr[i] = adr[i] & ~cidr[i]
    
    return adr

def main():
    adr_test=[10, 95, 56, 5]
    cidr_test=19
    mask_test=genere_masque(cidr_test)
    hostpart_test=hote_bit_a_bit(adr_test, mask_test)
    assert hostpart_test == [0,0,24,5]
    print("Test with valid numbers [OK]")

    adr_test=[10, 95, 56, -5]
    cidr_test=19
    mask_test=genere_masque(cidr_test)
    hostpart_test=hote_bit_a_bit(adr_test, mask_test)
    assert hostpart_test == None
    print("Test with invalid numbers #1 [OK]")

    adr_test=[10, 95, 256, 5]
    cidr_test=19
    mask_test=genere_masque(cidr_test)
    hostpart_test=hote_bit_a_bit(adr_test, mask_test)
    assert hostpart_test == None
    print("Test with invalid numbers #2 [OK]")

    adr_test=[10, 95, "yolo", 5]
    cidr_test=19
    mask_test=genere_masque(cidr_test)
    hostpart_test=hote_bit_a_bit(adr_test, mask_test)
    assert hostpart_test == None
    print("Test with invalid type in list (str) #1 [OK]")

    adr_test=[10, 95, [5, 6], 5]
    cidr_test=19
    mask_test=genere_masque(cidr_test)
    hostpart_test=hote_bit_a_bit(adr_test, mask_test)
    assert hostpart_test == None
    print("Test with invalid type in list (lst) #2 [OK]")

    adr_test=[10, 95, 2.0, 5]
    cidr_test=19
    mask_test=genere_masque(cidr_test)
    hostpart_test=hote_bit_a_bit(adr_test, mask_test)
    assert hostpart_test == None
    print("Test with invalid type in list (float) #3 [OK]")

    adr_test=[10, 95, 2]
    cidr_test=19
    mask_test=genere_masque(cidr_test)
    hostpart_test=hote_bit_a_bit(adr_test, mask_test)
    assert hostpart_test == None
    print("Test with invalid length [OK]")

    adr_test="2345"
    cidr_test=19
    mask_test=genere_masque(cidr_test)
    hostpart_test=hote_bit_a_bit(adr_test, mask_test)
    assert hostpart_test == None
    print("Test with invalid type (str) [OK]")

    adr_test="2345"
    cidr_test=19
    mask_test="0100111010100"
    hostpart_test=hote_bit_a_bit(adr_test, mask_test)
    assert hostpart_test == None
    print("Test with invalid cidr type (str) [OK]")
    

if __name__ == '__main__':
    main()