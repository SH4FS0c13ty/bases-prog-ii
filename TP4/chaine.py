#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Oct 22 11:05:14 2019

@author: wurbel
"""

import string

coll = { 'é': 'e', 'è' : 'e', 'ê' : 'e', 'ë' : 'e',  
         'É': 'E', 'È' : 'E', 'Ê' : 'E', 'Ë' : 'E',
         'ü' : 'u', 'ù' : 'u', 'û' : 'u',
         'Ü' : 'U', 'Ù' : 'U', 'Û' : 'U',
         'î' : 'i', 'ï' : 'i',
         'Î' : 'I', 'Ï' : 'I',
         'ô' : 'o', 'ö' : 'o',
         'Ô' : 'O', 'Ö' : 'O',
         'Ÿ' : 'Y', 'ÿ' : 'y',
         'à' : 'a', 'â' : 'a', 'ä' : 'a',
         'À' : 'A', 'Â' : 'A', 'Ä' : 'A',
         'Œ' : 'OE', 'œ' : 'oe', 'ç' : 'c', 'Ç' : 'C',
         'Æ' : 'AE', 'æ' : 'ae' }

def nettoie(chaine):
    """Nettoie une chaîne de caractères en
    - remplaçant les lettres accentuées par des lettres non acentuées
    - en remplaçant les ligatures (æ et œ) par leur lettres)
    - convertissant la chaîne en minuscules"""
    assert isinstance(chaine, str)
    
    res = ""
    for car in chaine:
        if car in string.ascii_letters \
            or car in string.digits \
            or car in string.whitespace \
            or car in string.punctuation or car == '&':
            res += car
        elif car in coll.keys():
            res += coll[car]
    return res.lower()

def main():
    print("Test de clean ", end='')
    assert nettoie("a b c d") == "a b c d"
    print('.', end='')
    s = nettoie("Dès Noël où un zéphyr haï me vêt de glaçons würmiens "  
                "je dîne d'exquis rôtis de bœuf au kir à l'aÿ d'âge mûr " 
                "& cætera !")
    assert (s == "des noel ou un zephyr hai me vet de "
                 "glacons wurmiens je dine d'exquis rotis de boeuf au kir "
                 "a l'ay d'age mur & caetera !")
    print('.', end='')
    
    print("[OK]")

if __name__ == "__main__":
    main()
