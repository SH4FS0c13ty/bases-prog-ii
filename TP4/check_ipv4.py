# -*- coding: utf-8 -*-
def check_ipv4(l):
    """ Check if the submitted address is a valid IPv4 address """
    flag=True
    if type(l) is not list:
        return False
    if len(l) != 4:
        flag=False
    for i in range(0,len(l)):
        if type(l[i]) is not int:
            flag=False
        elif l[i] < 0 or l[i] > 255:
            flag=False
       
    return flag
    

def main():
    ip_test=[102,54,255,8]
    var=check_ipv4(ip_test)
    assert var == True
    print("IP Check with valid numbers [OK]")
    
    ip_test=[-102,54,255,8]
    var=check_ipv4(ip_test)
    assert var == False
    print("IP Check with invalid numbers [OK]")
    
    ip_test=["bruh",54,255,8]
    var=check_ipv4(ip_test)
    assert var == False
    print("IP Check with invalid type str [OK]")
    
    ip_test=[[102,5],54,255,8]
    var=check_ipv4(ip_test)
    assert var == False
    print("IP Check with invalid type lst [OK]")
    
    ip_test=[102.0,54,255,8]
    var=check_ipv4(ip_test)
    assert var == False
    print("IP Check with invalid type float [OK]")
    
    ip_test=[54,255,8]
    var=check_ipv4(ip_test)
    assert var == False
    print("IP Check with invalid length [OK]")

    ip_test=5
    var=check_ipv4(ip_test)
    assert var == False
    print("IP Check with invalid submitted type [OK]")

    

if __name__ == '__main__':
    main()