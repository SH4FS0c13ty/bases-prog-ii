# -*- coding: utf-8 -*-

import chaine

alphabet = "abcdefghijklmnopqrstuvwxyz"

def chiffre_cesar(txt, cle):
    """ Cipher the submitted text using Cesar cipher and submitted key """
    if type(txt) != str or type(cle) != int or cle > 26 or cle < 0:
        return None
    txt = chaine.nettoie(txt)
    finaltxt = ""
    for i in range(len(txt)):
        if txt[i] == " ":
            finaltxt+=txt[i]
        else:
            index = alphabet.find(txt[i])
            cindex = index+cle
            if cindex > 26:
                cindex = cindex - 26
            finaltxt+=alphabet[cindex]
    return finaltxt
    
def dechiffre_cesar(txt, cle):
    """ Decipher the submitted text using Cesar cipher and submitted key """
    if type(txt) != str or type(cle) != int or cle > 26 or cle < 0:
        return None
    txt = chaine.nettoie(txt)
    finaltxt = ""
    for i in range(len(txt)):
        if txt[i] == " ":
            finaltxt+=txt[i]
        else:
            index = alphabet.find(txt[i])
            cindex = index-cle
            if cindex < 0:
                cindex = cindex + 26
            finaltxt+=alphabet[cindex]
    return finaltxt


def main():
    txt_test="tqk"
    cle_test=12
    var=dechiffre_cesar(txt_test, cle_test)
    assert var == "hey"
    print("Cesar cipher with valid arguments #1 [OK]")
          
    txt_test="ebv"
    cle_test=23
    var=dechiffre_cesar(txt_test, cle_test)
    assert var == "hey"
    print("Cesar cipher with valid arguments #2 [OK]")
          
    txt_test="tqk tqk"
    cle_test=12
    var=dechiffre_cesar(txt_test, cle_test)
    assert var == "hey hey"
    print("Cesar cipher with valid arguments #3 [OK]")
    
    txt_test=12
    cle_test=12
    var=dechiffre_cesar(txt_test, cle_test)
    assert var == None
    print("Cesar cipher with invalid arguments #1 [OK]")
          
    txt_test="hey"
    cle_test="hey"
    var=dechiffre_cesar(txt_test, cle_test)
    assert var == None
    print("Cesar cipher with invalid arguments #2 [OK]")
          
    txt_test="hey"
    cle_test=27
    var=dechiffre_cesar(txt_test, cle_test)
    assert var == None
    print("Cesar cipher with invalid arguments #3 [OK]")
          
    txt_test=["h","e","y"]
    cle_test=12
    var=dechiffre_cesar(txt_test, cle_test)
    assert var == None
    print("Cesar cipher with invalid arguments #4 [OK]")
    

if __name__ == '__main__':
    main()
