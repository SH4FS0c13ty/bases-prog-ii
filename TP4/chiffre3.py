# -*- coding: utf-8 -*-

import chaine

alphabet = "abcdefghijklmnopqrstuvwxyz "

def chiffre_cesar(txt, cle):
    """ Cipher the submitted text using Cesar cipher and submitted key """
    if type(txt) != str or type(cle) != int or cle > 26 or cle < 0:
        return None
    txt = chaine.nettoie(txt)
    finaltxt = ""
    for i in range(len(txt)):
        if txt[i] == " ":
            finaltxt+=txt[i]
        else:
            index = alphabet.find(txt[i])
            cindex = index+cle
            if cindex > 26:
                cindex = cindex - 26
            finaltxt+=alphabet[cindex]
    return finaltxt
    
def dechiffre_cesar(txt, cle):
    """ Decipher the submitted text using Cesar cipher and submitted key """
    if type(txt) != str or type(cle) != int or cle > 26 or cle < 0:
        return None
    txt = chaine.nettoie(txt)
    finaltxt = ""
    for i in range(len(txt)):
        if txt[i] == " ":
            finaltxt+=txt[i]
        else:
            index = alphabet.find(txt[i])
            cindex = index-cle
            if cindex < 0:
                cindex = cindex + 26
            finaltxt+=alphabet[cindex]
    return finaltxt

def chiffre_vigenere(msg, cle):
    """ Cipher the submitted text using Vigenere cipher and submitted key """
    if type(msg) != str or type(cle) != str:
        return None
    msg = chaine.nettoie(msg)
    cle = chaine.nettoie(cle)
    cle = cle.replace(" ", "")
    finaltxt = ""
    keylen = len(cle)
    keyind = 0
    for i in range(len(msg)):
        if msg[i] == " ":
            finaltxt+=msg[i]
            keyind-=1
        else:
            if keyind >= keylen:
                keyind = 0
            lmsgindex = alphabet.find(msg[i])
            lcleindex = alphabet.find(cle[keyind])
            cindex = lmsgindex+lcleindex
            if cindex > 26:
                cindex = cindex - 26
            finaltxt+=alphabet[cindex]
            keyind+=1
    return finaltxt


def main():
    txt_test="Bonsoir"
    cle_test="Hey"
    var=chiffre_vigenere(txt_test, cle_test)
    assert var == "islzsgy"
    print("Vigenere cipher with valid arguments #1 [OK]")
          
    txt_test="Bonsoir Bonsoir"
    cle_test="Hey"
    var=chiffre_vigenere(txt_test, cle_test)
    assert var == "islzsgy islzsgy"
    print("Vigenere cipher with valid arguments #2 [OK]")
    
    txt_test="Bonsoir"
    cle_test="Hey hey"
    var=chiffre_vigenere(txt_test, cle_test)
    assert var == "islzsgy"
    print("Vigenere cipher with valid arguments #3 [OK]")
          
    txt_test=12
    cle_test="hey"
    var=chiffre_vigenere(txt_test, cle_test)
    assert var == None
    print("Vigenere cipher with invalid arguments #1 [OK]")
          
    txt_test="hey"
    cle_test=12
    var=chiffre_vigenere(txt_test, cle_test)
    assert var == None
    print("Vigenere cipher with invalid arguments #2 [OK]")
          
    txt_test=["h","e","y"]
    cle_test=12
    var=chiffre_vigenere(txt_test, cle_test)
    assert var == None
    print("Vigenere cipher with invalid arguments #3 [OK]")
    

if __name__ == '__main__':
    main()
