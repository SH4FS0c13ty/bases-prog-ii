# -*- coding: utf-8 -*-
import check_ipv4

def ipv4_vers_chaine(l):
    """ Transform IPv4 address into string with format xxx.xxx.xxx.xxx """
    ip=None
    check_ip=check_ipv4.check_ipv4(l)
    if check_ip == False:
        return ip
    for i in range(0,len(l)):
        if i != 0:
            ip=ip + "." + str(l[i])
        else:
            ip=str(l[i])
    return ip

def chaine_vers_ipv4(s):
    """ Transform IPv4 string into list with format [xxx,xxx,xxx,xxx] """
    if type(s) != str:
        return None
    for char in s:
        if char not in string.digits and char != ".":
            return None
    ip = s.split('.')
    if len(ip) != 4:
        return None
    for i in range(0,len(ip)):
        ip[i]=int(ip[i])
    return ip

def main():
    ip_test="124.33.113.5"
    ip=chaine_vers_ipv4(ip_test)
    print("Tests with valid numbers checking")
    assert ip == [124,33,113,5]
    print("IP to string with correct numbers [OK]")

    ip_test="124.6.-33.bruh.[]"
    ip=chaine_vers_ipv4(ip_test)
    assert ip == None
    print("IP to string with int and non int [OK]")

    ip_test=56
    ip=chaine_vers_ipv4(ip_test)
    assert ip == None
    print("IP to string with invalid submitted type [OK]")
 

if __name__ == '__main__':
    main()
