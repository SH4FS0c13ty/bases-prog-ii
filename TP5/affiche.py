# -*- coding: utf-8 -*-

import os

def cat(file):
    """ Read the file and display it line by line """
    # Arguments:
    # file: File to submit
    
    # Check submitted argument and if file exists
    if type(file) != str:
        return None
    elif os.path.isfile(file) == False:
        print("File does not exist:", file)
    else:
        # Read the file line by line
        with open(file, "r", encoding="utf-8") as opened_file:
            line = opened_file.readline()
            while line != "":
                line.rstrip("\n")
                print(line)
                line = opened_file.readline()

    return None

def main():
    file_path = input("Enter the file path: ")
    cat(file_path)

if __name__ == "__main__":
    main()
