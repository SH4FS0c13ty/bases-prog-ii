#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os, sys

def cat(file):
    """ Read the file and display it line by line """
    # Arguments:
    # file: File to submit
    
    # Check submitted argument and if file exists
    if type(file) != str:
        return None
    elif os.path.isfile(file) == False:
        print("File does not exist:", file)
    else:
        # Read the file line by line
        with open(file, "r", encoding="utf-8") as opened_file:
            line = opened_file.readline()
            while line != "":
                line.rstrip("\n")
                print(line)
                line = opened_file.readline()

    return None

def main():
    arglen = len(sys.argv)
    for i in range(1, arglen):
        cat(sys.argv[i])

if __name__ == "__main__":
    main()
