#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os, sys

def resoud(host):
    """ Find the host IP in /etc/hosts file """
    # Arguments:
    # host: Hostname to submit
    
    # file = "bonsoir.txt" # Test file with one valid line as hosts file
    file = "/etc/hosts"
    
    # Check submitted argument and if file exists
    if type(host) != str:
        return None
    elif os.path.isfile(file) == False:
        print("File /etc/hosts could not be found.")
    else:
        # Read the file line by line
        with open(file, "r", encoding="utf-8") as opened_file:
            line = opened_file.readline()
            while line != "":
                if line[0] != "#":
                    cur = line.find(host)
                    if cur != -1 and (line.endswith(host+"\n") == True or line.endswith(host) == True):
                        line = line[:cur]
                        for i in line:
                            if line[-1] == " ":
                                line = line[:-1]
                        return line
                line = opened_file.readline()

    return None

def main():
    arglen = len(sys.argv)
    if arglen != 2:
        print("Invalid argument length.")
        print("Syntax: resolution.py hostname")
    else:
        ip = resoud(sys.argv[1])
        if ip != None:
            print(ip)
        

if __name__ == "__main__":
    main()
