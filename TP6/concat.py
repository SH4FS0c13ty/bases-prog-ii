#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os, sys

def verifie_fichier(chemin):
    """ Check file path """
    # Arguments:
    # chemin: File path to submit
    
    flag = False
    
    # Check the file path
    if type(chemin) == str and os.path.isfile(chemin) == True:
        flag = True
    elif type(chemin) == str and os.path.isdir(chemin) == True:
        print("Not a file:", chemin)
    else:
        print("File not found:", chemin)
    
    return flag

def copie(fd_lect, fd_ecr, mode="w"):
    """ Copy content of a file to another """
    # Arguments:
    # fd_lect: File to read
    # fd_ecr: File to write
    # mode: File write mode [w|a]
    
    # Check the arguments
    if type(fd_lect) != str or type(fd_ecr) != str or type(mode) != str:
        return None
    elif mode != "w" and mode != "a":
        return None
    
    with open(fd_lect, "r", encoding="utf-8") as fd_lect_open:
        with open(fd_ecr, mode, encoding="utf-8") as fd_ecr_open:
            rline = fd_lect_open.readline()
            while rline != "":
                fd_ecr_open.write(rline)
                rline = fd_lect_open.readline()
            fd_ecr_open.write("\n")

    return None

def double_file_check(file1, file2):
    """ Check 2 files at a time """
    # Arguments:
    # file1: First file to submit
    # file2: Second file to submit
    
    check_res = True
    
    # Check if the files exists
    check1 = verifie_fichier(file1)
    check2 = verifie_fichier(file2)

    if check1 != True or check2 != True:
        check_res = False

    return check_res

def main():
    arglen = len(sys.argv)
    if arglen != 4:
        print("Invalid argument length.")
        print("Syntax: concat.py file1 file3 output")
        return None

    full_check = double_file_check(sys.argv[1], sys.argv[2])
    if full_check != True:
        return None
    copie(sys.argv[1], sys.argv[3], "w")
    copie(sys.argv[2], sys.argv[3], "a")

if __name__ == "__main__":
    main()
