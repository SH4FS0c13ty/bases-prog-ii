#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os, sys

def verifie_fichier(chemin):
    """ Check file path """
    # Arguments:
    # chemin: File path to submit
    
    flag = False
    
    # Check the file path
    if type(chemin) == str and os.path.isfile(chemin) == True:
        flag = True
    elif type(chemin) == str and os.path.isdir(chemin) == True:
        print("Not a file:", chemin)
    else:
        print("File not found:", chemin)
    
    return flag

def copie(file1, file2, output):
    """ Copy content of a 2 files to one another using TAB as separator """
    # Arguments:
    # file1: First file to submit
    # file2: Second file to submit
    # output: Output file
    
    # Check the arguments
    if type(file1) != str or type(file2) != str or type(output) != str:
        return None
    
    with open(output, "w", encoding="utf-8") as output_open:
        with open(file1, "r", encoding="utf-8") as file1_open:
            with open(file2, "r", encoding="utf-8") as file2_open:
                rline1 = file1_open.readline()
                rline2 = file2_open.readline()
                while rline1 != "" or rline2 != "":
                    output_open.write(rline1.rstrip("\n") + "\t" + rline2.rstrip("\n") + "\n")
                    rline1 = file1_open.readline()
                    rline2 = file2_open.readline()

    return None

def double_file_check(file1, file2):
    """ Check 2 files at a time """
    # Arguments:
    # file1: First file to submit
    # file2: Second file to submit
    
    check_res = True
    
    # Check if the files exists
    check1 = verifie_fichier(file1)
    check2 = verifie_fichier(file2)

    if check1 != True or check2 != True:
        check_res = False

    return check_res

def main():
    arglen = len(sys.argv)
    if arglen != 4:
        print("Invalid argument length.")
        print("Syntax: paste.py file1 file3 output")
        return None

    full_check = double_file_check(sys.argv[1], sys.argv[2])
    if full_check != True:
        return None
    copie(sys.argv[1], sys.argv[2], sys.argv[3])

if __name__ == "__main__":
    main()
