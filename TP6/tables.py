# -*- coding: utf-8 -*-

def tables_gen(file):
    """ Génère les tables de multiplications dans un fichier """
    # Arguments:
    # file: File to write
    
    # Check submitted argument and if file exists
    if type(file) != str:
        return None
    
    
    # Read the file line by line
    with open(file, "w", encoding="utf-8") as opened_file:
        for i in range(2,10):
            for j in range(1, 10):
                opened_file.write(str(i) + " * " + str(j) + " = " + str(i*j) + "\n")
            opened_file.write("\n")
            

    return None

def main():
    tables_gen("tables.txt")

if __name__ == "__main__":
    main()
