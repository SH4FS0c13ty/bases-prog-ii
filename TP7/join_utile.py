#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os, sys

def verifie_fichier(chemin):
    """ Check file path """
    # Arguments:
    # chemin: File path to submit
    
    flag = False
    
    # Check the file path
    if type(chemin) == str and os.path.isfile(chemin) == True:
        flag = True
    elif type(chemin) == str and os.path.isdir(chemin) == True:
        print("Not a file:", chemin)
    else:
        print("File not found:", chemin)
    
    return flag
    
def double_file_check(file1, file2):
    """ Check 2 files at a time """
    # Arguments:
    # file1: First file to submit
    # file2: Second file to submit
    
    check_res = True
    
    # Check if the files exists
    check1 = verifie_fichier(file1)
    check2 = verifie_fichier(file2)

    if check1 != True or check2 != True:
        check_res = False

    return check_res

def lire_ligne(fd, liste, ajout):
    """ Read a line in a file"""
    # Arguments:
    # fd: File descriptor
    # liste: List to store splitted expressions
    # ajout: Add to or overwrite the list [True|False] True to Add, False to Overwrite
    
    # Check the arguments
    if type(liste) != list or type(ajout) != bool:
        return None
    
    line = fd.readline()
    if line == "":
        return False
    if line.endswith("\n"):
        flag = True
    else:
        flag = False
    line = line.strip("\n")
    if ajout == False:
        liste.clear()
    liste.append(line.split())
    
    return flag

def main():
    file1 = "bonsoir.txt"
    file2 = "test.txt"
    
    full_check = double_file_check(file1, file2)
    if full_check != True:
        return None
    
    liste = []
    with open(file1, "r", encoding="utf-8") as file1_open:
        res = True
        while res == True:
            res = lire_ligne(file1_open, liste, False)
    assert res == False
    print("Function reached EOF [OK]")
    
    liste = []
    with open(file1, "r", encoding="utf-8") as file1_open:
        res = lire_ligne(file1_open, liste, False)
        assert res == True
        print("Read first line [OK]")
        res = lire_ligne(file1_open, liste, False)
        assert res == False
        print("Read second (last) line [OK]")

if __name__ == "__main__":
    main()
