#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os, sys

def verifie_fichier(chemin):
    """ Check file path """
    # Arguments:
    # chemin: File path to submit
    
    flag = False
    
    # Check the file path
    if type(chemin) == str and os.path.isfile(chemin) == True:
        flag = True
    elif type(chemin) == str and os.path.isdir(chemin) == True:
        print("Not a file:", chemin)
    else:
        print("File not found:", chemin)
    
    return flag
    
def double_file_check(file1, file2):
    """ Check 2 files at a time """
    # Arguments:
    # file1: First file to submit
    # file2: Second file to submit
    
    check_res = True
    
    # Check if the files exists
    check1 = verifie_fichier(file1)
    check2 = verifie_fichier(file2)

    if check1 != True or check2 != True:
        check_res = False

    return check_res

def lire_ligne(fd, liste, ajout):
    """ Read a line in a file"""
    # Arguments:
    # fd: File descriptor
    # liste: List to store splitted expressions
    # ajout: Add to or overwrite the list [True|False] True to Add, False to Overwrite
    
    # Check the arguments
    if type(liste) != list or type(ajout) != bool:
        return None
    
    line = fd.readline()
    if line == "":
        return False
    if line.endswith("\n"):
        flag = True
    else:
        flag = False
    line = line.strip("\n")
    if ajout == False:
        liste.clear()
    liste.append(line.split())
    
    return flag

def ecrit_ligne(ligne1, ligne2, fd):
    """ Joins 2 lines with a TAB in a file """
    # Arguments:
    # ligne1: First line to submit
    # ligne2: Second line to submit
    # fd: File descriptor to submit
    
    # Check the submitted arguments
    if (type(ligne1) != list or type(ligne2) != list) and (ligne1 != "" and ligne2 != ""):
        return None
    elif ligne1 == "" and ligne2 == "":
        return None
    elif ligne1 == "" or ligne2 == "":
        if ligne1 == "":
            ligne1 = ["", ""]
        elif ligne2 == "":
            ligne2 = ["", ""]
    
    # Checking cases and executing according actions
    if ligne1[0] == ligne2[0]:
        fd.write(str(ligne1[0]) + "\t" + str(ligne1[1]) + "\t" + str(ligne2[1]) + "\n")
    elif ligne1[0] == "":
        fd.write(str(ligne2[0]) + "\t\t" + str(ligne2[1]) + "\n")
    elif ligne2[0] == "":
        fd.write(str(ligne1[0]) + "\t" + str(ligne1[1]) + "\n")
    else:
        return None
        #fd.write(str(ligne1[0]) + "\t" + str(ligne2[1]))
        #fd.write(str(ligne2[0]) + "\t" + str(ligne2[1]))
    return None

def main():
    ecrit_ligne(["a", "1"], ["a", "X"], sys.stdout)
    ecrit_ligne(["b", "2"], "", sys.stdout)
    ecrit_ligne("", ["f", "Z"], sys.stdout)
    ecrit_ligne("", "", sys.stdout)

if __name__ == "__main__":
    main()
